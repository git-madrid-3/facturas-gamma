public interface iFactura
{

    public void cambiarEstado(Estados estado);
	
	public float getTotal();
	
	public float getCantidadIva();
	
}